SC.initialize({
	client_id: 'bf7b197bbeee57cfd00d470ece91a79d',
});

// Getting all the Questionmarks' tracks
SC.get('users/5612035/tracks').then(function(tracks){
	$.each(tracks, function(index,track) {

		var songid = parseInt(index + 1);
		var song = $('<div id="song_' + songid + '" title="' + track.title + '" ></div>');
		$('#songs').append(song);
		$(song).addClass('song');

		// Get each song's artwork
		var art = track.artwork_url;
		if (art == null) {
			art = track.user.avatar_url;
		}
		// We need the crop size not the default large so we replace it in the URL string
		var rg = /large/g;
		var artCrop = art.replace(rg,'crop');
		// And style it
		$(song).css({
			'background-image':'url(' + artCrop + ')',
			'background-size':'cover'
		});

		// Create overlay div that will appear on hover and will contain song title and player widget
		var overlay = $('<div class="overlay"></div>');
		overlay.html('<h3 class="song_title">' + track.title + '</div>')

		song.html(overlay);


	});
});


$(document).ready( function() {

	// If the follow button is pressed (temporary behavior until uploaded to demo server - local redirect URI cannot be reached by SoundCloud)
	$('#followUs').click( function() {
		window.open('https://soundcloud.com/questionmarksband','_blank');
}	);
});

	
	
